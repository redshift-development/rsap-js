/* -------------------------------------------------------------------------------------------
 * Copyright (c) RedShift Development LLC
 * Licensed under the MIT license. See License.txt in the project root for license information
 * ---------------------------------------------------------------------------------------- */

interface IpcClient {
  invoke(channel: 'rsap.ipc.service.connect', name: string): Promise<string>
  invoke(channel: 'rsap.ipc.service.send', name: string, data: string): Promise<Error | null>
  invoke(channel: 'rsap.ipc.service.close', name: string, id: string): Promise<boolean>
  on(channel: 'rsap.ipc.service.out', listener: (event: Event, data: string) => void): IpcClient
  on(channel: 'rsap.ipc.service.err', listener: (event: Event, data: string) => void): IpcClient
  on(channel: 'rsap.ipc.service.closed', listener: (event: Event, reason: Error | null) => void): IpcClient
}

export interface BridgeClient {
  connect(): Promise<Error | null>
  send(data: string): Promise<Error | null>
  close(): Promise<Error | null>
  onOut(handler: (data: string) => void): void
  onErr(handler: (data: string) => void): void
  onClose(handler: (reason: Error | null) => void): void
}

export const BridgeConnection = (name: string, ipc: IpcClient): BridgeClient => {
  let clientId = ''
  let outHandler = (data: string) => console.log(data)
  let errHandler = (data: string) => console.error(data)
  let closeHandler = (reason: Error | null) => console.error(`service '${name}' closed:`, reason)
  const connect = async () => {
    const id = await ipc.invoke('rsap.ipc.service.connect', name)
    if (id === '') {
      return new Error(`unable to connect to service '${name}'`)
    }
    clientId = id
    return null
  }
  ipc.on('rsap.ipc.service.out', (_, data) => {
    outHandler(data)
  })
  ipc.on('rsap.ipc.service.err', (event, data) => {
    errHandler(data)
  })
  ipc.on('rsap.ipc.service.closed', (event, reason) => {
    closeHandler(reason)
  })
  const send = async (data: string) => {
    return ipc.invoke('rsap.ipc.service.send', name, data)
  }
  const close = async () => {
    const success = await ipc.invoke('rsap.ipc.service.close', name, clientId)
    if (success) {
      return null
    }
    return new Error(`unable to close client connection to service '${name}'`)
  }
  const onOut = (handler: (data: string) => void) => {
    outHandler = handler
  }
  const onErr = (handler: (data: string) => void) => {
    errHandler = handler
  }
  const onClose = (handler: (reason: Error | null) => void) => {
    closeHandler = handler
  }
  return {
    connect,
    send,
    close,
    onOut,
    onErr,
    onClose,
  }
}
