/* -------------------------------------------------------------------------------------------
 * Copyright (c) RedShift Development LLC
 * Licensed under the MIT license. See License.txt in the project root for license information
 * ---------------------------------------------------------------------------------------- */
export const BridgeConnection = (name, ipc) => {
    let clientId = '';
    let outHandler = (data) => console.log(data);
    let errHandler = (data) => console.error(data);
    let closeHandler = (reason) => console.error(`service '${name}' closed:`, reason);
    const connect = async () => {
        const id = await ipc.invoke('rsap.ipc.service.connect', name);
        if (id === '') {
            return new Error(`unable to connect to service '${name}'`);
        }
        clientId = id;
        return null;
    };
    ipc.on('rsap.ipc.service.out', (_, data) => {
        outHandler(data);
    });
    ipc.on('rsap.ipc.service.err', (event, data) => {
        errHandler(data);
    });
    ipc.on('rsap.ipc.service.closed', (event, reason) => {
        closeHandler(reason);
    });
    const send = async (data) => {
        return ipc.invoke('rsap.ipc.service.send', name, data);
    };
    const close = async () => {
        const success = await ipc.invoke('rsap.ipc.service.close', name, clientId);
        if (success) {
            return null;
        }
        return new Error(`unable to close client connection to service '${name}'`);
    };
    const onOut = (handler) => {
        outHandler = handler;
    };
    const onErr = (handler) => {
        errHandler = handler;
    };
    const onClose = (handler) => {
        closeHandler = handler;
    };
    return {
        connect,
        send,
        close,
        onOut,
        onErr,
        onClose,
    };
};
//# sourceMappingURL=client.js.map