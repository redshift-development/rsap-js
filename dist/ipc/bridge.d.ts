/// <reference types="node" />
import { ChildProcessByStdio } from 'child_process';
import { Writable, Readable } from 'stream';
declare type ChildProcess = ChildProcessByStdio<Writable, Readable, Readable>;
interface IpcClient {
    send(channel: 'rsap.ipc.service.out', data: string): void;
    send(channel: 'rsap.ipc.service.err', data: string): void;
    send(channel: 'rsap.ipc.service.closed', reason: Error | null): void;
}
interface IpcService {
    handle(channel: 'rsap.ipc.service.connect', listener: (event: IpcEvent, name: string) => string): void;
    handle(channel: 'rsap.ipc.service.send', listener: (event: IpcEvent, name: string, data: string) => Error | null): void;
    handle(channel: 'rsap.ipc.service.close', listener: (event: IpcEvent, name: string, id: string) => boolean): void;
}
interface IpcEvent extends Event {
    sender: IpcClient;
}
export interface IServiceBridge {
    connect(name: string, process: ChildProcess): void;
    close(name: string, reason: Error | null): void;
}
export declare const ServiceBridge: (ipc: IpcService) => IServiceBridge;
export {};
