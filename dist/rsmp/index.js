/* -------------------------------------------------------------------------------------------
 * Copyright (c) RedShift Development LLC
 * Licensed under the MIT license. See License.txt in the project root for license information
 * ---------------------------------------------------------------------------------------- */
function makeId() {
    let result = '';
    for (let i = 16; i > 0; --i) {
        result += '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'[Math.floor(Math.random() * 62)];
    }
    return result;
}
export const RsmpService = (name, client) => {
    const requests = new Map();
    const eventHandlers = new Map();
    const onceHandlers = new Map();
    const handleResponse = (msg) => {
        const req = requests.get(msg.id);
        if (req !== undefined) {
            if (msg.error !== undefined) {
                req.reject(msg.error);
            }
            else {
                req.resolve(msg.data);
            }
            requests.delete(msg.id);
        }
    };
    const handleEvent = (msg) => {
        const eventHandler = eventHandlers.get(msg.action);
        if (eventHandler !== undefined) {
            eventHandler(msg.data);
        }
        const onceHandler = onceHandlers.get(msg.action);
        if (onceHandler !== undefined) {
            onceHandler(msg.data);
            onceHandlers.delete(msg.action);
        }
    };
    const handleError = (id, reason) => {
        const req = requests.get(id);
        if (req !== undefined) {
            req.reject({
                code: 201,
                message: reason.message,
                data: null,
            });
            requests.delete(id);
        }
    };
    client.onOut((data) => {
        const msg = JSON.parse(data);
        if (msg.id !== '') {
            handleResponse(msg);
        }
        else {
            handleEvent(msg);
        }
    });
    const request = (action, data) => {
        return new Promise((resolve, reject) => {
            const id = makeId();
            requests.set(id, { resolve, reject });
            client
                .send(JSON.stringify({
                id: id,
                rsmp: '1.0',
                action: action,
                data: data,
            }))
                .then((err) => {
                if (err != null) {
                    handleError(id, new Error('Failed to send message to service: ' + err.message));
                }
            });
        });
    };
    const emit = (action, data) => {
        return new Promise((resolve, reject) => {
            client
                .send(JSON.stringify({
                rsmp: '1.0',
                action: action,
                data: data,
            }))
                .then((err) => {
                if (err !== null) {
                    reject(new Error('Failed to send message to service: ' + err.message));
                }
                else {
                    resolve(null);
                }
            });
        });
    };
    const on = (event, handler) => {
        eventHandlers.set(event, handler);
    };
    const once = (event, handler) => {
        onceHandlers.set(event, handler);
    };
    return {
        request,
        emit,
        on,
        once,
    };
};
//# sourceMappingURL=index.js.map