# RSAP-JS

This is a dev repo of the RedShift Application Protocol. While the project is
in a working and usable state, it is to be considered unstable. Use at your own risk.

This is an application messaging protocol developed for use in electron applications.
It is inspired by Microsoft's Language Server Protocol and uses JSON-RPC style
messaging to allow an electron client UI to communicate with an underlying service
over STD in, out, and err. There is a Golang service side available at [https://gitlab.com/codydbentley/rsap-go](https://gitlab.com/codydbentley/rsap-go).

## Usage

### Electron
This example is for spawning an RSAP-enabled child process that communicates over std IO.

`background.js/ts`:
```javascript
import { ipcMain } from "electron";
import { ServiceBridge } from "rsap-js/dist/ipc/bridge"
const { spawn } = require('child_process');

const bridge = ServiceBridge(ipcMain)
const srvChild = spawn('path/to/child/process.exe')
bridge.connect('service_name', srvChild)
```

`preload.js` (needed for `nodeIntegration: false`):
```javascript
import { ipcRenderer } from 'electron'
import { BridgeConnection } from 'rsap-js/dist/ipc/client'

window.serviceBridge = BridgeConnection('service_name', ipcRenderer)

window.serviceBridge.onErr(data => {
  console.log('service_name:', data)
})
```

`index.js/ts`:
```typescript
import { RsmpService } from 'rsap-js/dist/rsmp'
import { BridgeClient } from 'rsap-js/dist/ipc/client'

// declarations only needed for TS
declare interface RsmpWindow extends Window {
  serviceBridge: BridgeClient
}
declare const window: RsmpWindow

const MyService = RsmpService('service_name', window.serviceBridge)
try {
  const res = await MyService.request('test.echo', { someMessage: "Hello world!" })
  console.log('response', res)
} catch (e) {
  console.error('failure:', e)
}
```